import java.util.Random;

public class RouletteWheel {
    private Random randNum;
    private int number;

    public RouletteWheel()
    {
        randNum = new Random();
        number = 0;
    }

    public void spin()
    {
        this.number = this.randNum.nextInt(37);
    }

    public int getValue()
    {
        return this.number;
    }
}
