import java.util.Scanner;
public class Roulette {
    public static void main(String[] args)
    {
        int money = 1000;

        Scanner scan = new Scanner(System.in);

        boolean bet = false;
        boolean done = false;

        RouletteWheel wheel = new RouletteWheel();

        int won = 0;
        int lost = 0;
        
        while(!bet)
        {
            System.out.println("Would you like to make a bet?");
            System.out.println("Answer with yes or no.");
            String answer = scan.next().toLowerCase();

            if(answer.equals("yes"))
            {
                bet = true;
            }
            else if(answer.equals("no"))
            {
                bet = true;
                done = true;
            }
        }
            
        while(!done)
        {
            int betNumber = 37;
            while(betNumber<0 || betNumber>36)
            {
                System.out.println("Which number would you like to bet on, between 0 and 36?");
                betNumber = scan.nextInt();
            }

            int betAmount = 1001;
            while(betAmount >= money)
            {
                System.out.println("How much money would you like to bet? It cannot be more than $" + money);
                betAmount = scan.nextInt();
            }

            wheel.spin();
            
            if(betNumber == wheel.getValue())
            {
                int reward = betAmount * 35;
                won += reward;
                System.out.println("The number is " + wheel.getValue() + ". You won $" + reward + "!");
            }
            else
            {
                money = money - betAmount;
                lost += betAmount;
                System.out.println("The number is " + wheel.getValue() + ". You lost $" + betAmount);
            }

            String playAgain = "";

            while(!playAgain.equals("yes") && !playAgain.equals("no"))
            {
                System.out.println("Would you like to play again?");
                playAgain = scan.next().toLowerCase();

                if(playAgain.equals("no"))
                {
                    done = true;
                    System.out.println("You have won $" + won + " and lost $" + lost);
                }
            }

        }
        

    }
}